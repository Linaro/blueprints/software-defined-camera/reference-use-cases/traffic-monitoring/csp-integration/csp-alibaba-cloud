import json
from aliyunsdkvod.request.v20170321.GetVideoListRequest import GetVideoListRequest
from aliyunsdkvod.request.v20170321.GetURLUploadInfosRequest import GetURLUploadInfosRequest
from aliyunsdkvod.request.v20170321.GetPlayInfoRequest import GetPlayInfoRequest
from aliyunsdkvod.request.v20170321.DeleteVideoRequest import DeleteVideoRequest
import time as time1
import datetime


class Apsara_vod_utils:
    def __init__(self, vod_client):
        self.vod_client = vod_client
        
    def delete_video(self, video_id):
        request1 = DeleteVideoRequest()
        request1.set_accept_format('json')
        request1.set_VideoIds(video_id)
        response1 = self.vod_client.do_action_with_exception(request1)
        time1.sleep(2)

    def delete_selected_videos(self, start_time, end_time):
        request1 = GetVideoListRequest()
        request1.set_accept_format('json')
        video_list = []
        page = 1
        while(page <= 50):
            request1.set_PageNo(page)
            request1.set_PageSize(100)
            response1 = self.vod_client.do_action(request1)
            response1 = json.loads(response1.decode("utf-8"))

            if "VideoList" in response1:
                i = 1
                var = response1

                for vid_data in var['VideoList']['Video']:
                    present_time = vid_data['CreateTime']
                    present_time = datetime.datetime.strptime(
                        present_time, "%Y-%m-%d %H:%M:%S")
                    in_time = datetime.datetime.strptime(
                        start_time, "%Y-%m-%d %H:%M:%S")
                    out_time = datetime.datetime.strptime(
                        end_time, "%Y-%m-%d %H:%M:%S")
                    if(present_time > in_time and present_time < out_time):
                        video_list.append(present_time)
                        video_list.append(vid_data['Title'])
                        video_id = vid_data['VideoId']
                        request2 = GetPlayInfoRequest()
                        request2.set_accept_format('json')
                        request2.set_VideoId(video_id)
                        response2 = self.vod_client.do_action_with_exception(
                            request2)
                        var = json.loads(str(response2, encoding='utf-8'))
                        for vid_data in var['PlayInfoList']['PlayInfo']:
                            video_list.append(vid_data['PlayURL'])
                        i += 1

                        video_list.append(video_id)
                        request3 = DeleteVideoRequest()
                        request3.set_accept_format('json')

                        request3.set_VideoIds(video_id)
                        response3 = self.vod_client.do_action_with_exception(
                            request3)

            page += 1

        time1.sleep(2)

        video_chunks = [video_list[x:x+4]
                        for x in range(0, len(video_list), 4)]

        headings = ("Video", "Title",
                    "Date and Time (Shanghai)", "Delete Video")
        video_table_list = tuple(tuple(sub) for sub in video_chunks)
        len_list = int(len(video_list)/4)
        return headings, video_table_list, len_list

    def search_videos(self, start_time, end_time):
        request1 = GetVideoListRequest()
        request1.set_accept_format('json')
        video_list = []
        page = 1
        while(page <= 50):
            request1.set_PageNo(page)
            request1.set_PageSize(100)
            response1 = self.vod_client.do_action(request1)
            response1 = json.loads(response1.decode("utf-8"))

            if "VideoList" in response1:
                i = 1
                var = response1

                for vid_data in var['VideoList']['Video']:
                    present_time = vid_data['CreateTime']
                    present_time = datetime.datetime.strptime(
                        present_time, "%Y-%m-%d %H:%M:%S")
                    in_time = datetime.datetime.strptime(
                        start_time, "%Y-%m-%d %H:%M:%S")
                    out_time = datetime.datetime.strptime(
                        end_time, "%Y-%m-%d %H:%M:%S")
                    if(present_time > in_time and present_time < out_time):
                        video_list.append(present_time)
                        video_list.append(vid_data['Title'])
                        video_id = vid_data['VideoId']
                        request2 = GetPlayInfoRequest()
                        request2.set_accept_format('json')
                        request2.set_VideoId(video_id)
                        response2 = self.vod_client.do_action_with_exception(
                            request2)
                        var = json.loads(str(response2, encoding='utf-8'))
                        for vid_data in var['PlayInfoList']['PlayInfo']:
                            video_list.append(vid_data['PlayURL'])
                        i += 1

                        video_list.append(video_id)

            page += 1
        video_chunks = [video_list[x:x+4]
                        for x in range(0, len(video_list), 4)]
        headings = ("Video", "Title",
                    "Date and Time (Shanghai)", "Delete Video")
        video_table_list = tuple(tuple(sub) for sub in video_chunks)
        len_list = int(len(video_list)/4)
        return headings, video_table_list, len_list

    def delete_previous_videos(self):
        request1 = GetVideoListRequest()
        request1.set_accept_format('json')
        video_list = []
        page = 50
        while(page > 0):
            request1.set_PageNo(page)
            request1.set_PageSize(100)
            response1 = self.vod_client.do_action(request1)
            response1 = json.loads(response1.decode("utf-8"))
            if "VideoList" in response1:
                i = 1
                var = response1
                for vid_data in var['VideoList']['Video']:
                    if(i <= 25 and page == 1):
                        print("Adding video:", i)
                        video_list.append(vid_data['CreateTime'])
                        video_list.append(vid_data['Title'])
                        video_id = vid_data['VideoId']
                        request2 = GetPlayInfoRequest()
                        request2.set_accept_format('json')
                        request2.set_VideoId(video_id)
                        response2 = self.vod_client.do_action_with_exception(request2)
                        var = json.loads(str(response2, encoding='utf-8'))
                        for vid_data in var['PlayInfoList']['PlayInfo']:
                            video_list.append(vid_data['PlayURL'])
                    else:
                        print("Deleting video:", i)
                        video_id = vid_data['VideoId']
                        request3 = DeleteVideoRequest()
                        request3.set_accept_format('json')
                        request3.set_VideoIds(video_id)
                        response3 = self.vod_client.do_action_with_exception(request3)
                    i += 1
                page -= 1
            else:
                page -= 1
        time1.sleep(2)

    def get_saved_videos_list(self):
        request1 = GetVideoListRequest()
        request1.set_accept_format('json')
        video_list = []
        page = 1
        while(page <= 50):
            request1.set_PageNo(page)
            request1.set_PageSize(100)
            response1 = self.vod_client.do_action(request1)
            response1 = json.loads(response1.decode("utf-8"))

            if "VideoList" in response1:
                i = 1
                var = response1
                for vid_data in var['VideoList']['Video']:
                    present_time = vid_data['CreateTime']
                    present_time = datetime.datetime.strptime(
                        present_time, "%Y-%m-%d %H:%M:%S")
                    video_list.append(present_time)
                    video_list.append(vid_data['Title'])
                    video_id = vid_data['VideoId']

                    request2 = GetPlayInfoRequest()
                    request2.set_accept_format('json')
                    request2.set_VideoId(video_id)
                    response2 = self.vod_client.do_action_with_exception(request2)
                    var = json.loads(str(response2, encoding='utf-8'))
                    for vid_data in var['PlayInfoList']['PlayInfo']:
                        video_list.append(vid_data['PlayURL'])
                    i += 1
                    video_list.append(video_id)
            page += 1
        video_chunks = [video_list[x:x+4] for x in range(0, len(video_list), 4)]
        headings = ("Video", "Title", "Date and Time (Shanghai)", "Delete Video")
        video_table_list = tuple(tuple(sub) for sub in video_chunks)
        len_list = int(len(video_list)/4)

        return headings, video_table_list, len_list